FROM alpine:3.18.3
RUN apk add --no-cache openjdk17
RUN apk add --no-cache tzdata
COPY build/libs/lesson4-1.0.8e2d5c5.jar /app/
COPY gradle.properties /app/gradle.properties
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/lesson4-1.0.8e2d5c5.jar"]
