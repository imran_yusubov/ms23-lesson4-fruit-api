package az.ingress.lesson4.config;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

@Configuration
public class ModelMapperConfig {

    @Primary
    @Bean(name = "modelMapper1")
    // @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ModelMapper modelMapper() {
        final ModelMapper modelMapper = new ModelMapper();
        System.out.println("1--" + modelMapper);
        return modelMapper;
    }

    @Bean(name = "modelMapper2")
    //  @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ModelMapper modelMapper1() {
        final ModelMapper modelMapper = new ModelMapper();
        System.out.println("2--" + modelMapper);
        return modelMapper;
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Test test() {
        System.out.println("Creating new instance of Test class");
        return new Test();
    }
}
