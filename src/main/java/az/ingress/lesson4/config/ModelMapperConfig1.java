package az.ingress.lesson4.config;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ModelMapperConfig1 {

    @Bean(name = "modelMapper3")
    // @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ModelMapper modelMapper3() {
        final ModelMapper modelMapper = new ModelMapper();
        System.out.println("2-1--" + modelMapper);
        return modelMapper;
    }

}
