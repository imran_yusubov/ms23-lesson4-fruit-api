package az.ingress.lesson4.design.patterns;

public class SingletonExample {

    private static SingletonExample instance;

    private SingletonExample() {

    }

    public static SingletonExample getInstance() {
        if (instance == null) {
            synchronized (SingletonExample.class) {
                if (instance == null)
                    instance = new SingletonExample();
            }
        }
        return instance;
    }

}
