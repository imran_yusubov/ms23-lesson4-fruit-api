package az.ingress.lesson4.dto;

import lombok.Data;

@Data
public class Lesson {

    private String name;
    private String mark;
}
