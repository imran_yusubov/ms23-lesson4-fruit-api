package az.ingress.lesson4.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Data
@ConfigurationProperties(prefix = "xegani")
public class Student {

    private String firstName;
    private String lastName;
    private Integer age;

    private List<String> lessons;

    private Map<String, Integer> marks;

    private Map<String, Lesson> grades;
}
