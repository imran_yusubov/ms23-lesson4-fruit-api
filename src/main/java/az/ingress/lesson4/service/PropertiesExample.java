package az.ingress.lesson4.service;

import az.ingress.lesson4.dto.Student;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class PropertiesExample {

    @Value("${xegani.first-name}")
    private String firstName;

    @Autowired
    private Student student;


    @PostConstruct
    void print() {
        System.out.println("The value of firstName: " + firstName);

        System.out.println("The value of student: " + student);

    }

}
