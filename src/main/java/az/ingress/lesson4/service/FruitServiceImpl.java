package az.ingress.lesson4.service;

import az.ingress.lesson4.domain.FruitEntity;
import az.ingress.lesson4.dto.FruitRequestDto;
import az.ingress.lesson4.dto.FruitResponseDto;
import az.ingress.lesson4.repository.FruitRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FruitServiceImpl implements FruitService {

    private final ModelMapper modelMapper;
    private final FruitRepository fruitRepository;

    FruitServiceImpl(FruitRepository fruitRepository, @Qualifier("modelMapper2") ModelMapper modelMapper) {
        System.out.println("Created an instance of fruit class " + this);
        this.fruitRepository = fruitRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<FruitResponseDto> list(Integer from, Integer to) {
        return fruitRepository.findAll()
                .stream()
                .map(fruitEntity -> modelMapper.map(fruitEntity, FruitResponseDto.class))
                .toList();
    }

    @Override
    public FruitResponseDto get(Long id) {
        return fruitRepository.findById(id)
                .map(fruitEntity -> modelMapper.map(fruitEntity, FruitResponseDto.class))
                .orElseThrow(() -> new RuntimeException("Fruit not found"));
    }

    @Override
    public FruitResponseDto create(FruitRequestDto fruitDto) {
        FruitEntity fruit = modelMapper.map(fruitDto, FruitEntity.class);
        return modelMapper.map(fruitRepository.save(fruit), FruitResponseDto.class);
    }

    @Override
    public FruitResponseDto update(Long id, FruitRequestDto fruitDto) {
        final FruitEntity fruitEntity = modelMapper.map(fruitDto, FruitEntity.class);
        fruitEntity.setId(id);
        fruitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fruit with id %d not found".formatted(id)));
        return modelMapper.map(fruitRepository.save(fruitEntity), FruitResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        final FruitEntity fruit = fruitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fruit with id %d not found".formatted(id)));
        fruitRepository.delete(fruit);
    }
}
