package az.ingress.lesson4;

import az.ingress.lesson4.config.Test;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson4Application implements CommandLineRunner {

    private final Test test;
    private final Test test1;
    private final Test test3;

    private final ModelMapper modelMapper;

    public Lesson4Application(Test test, Test test1, Test test3,
                              @Qualifier("modelMapper3")
                              ModelMapper modelMapper) {
        this.test = test;
        this.test1 = test1;
        this.test3 = test3;
        this.modelMapper = modelMapper;
    }

    public static void main(String[] args) {
        SpringApplication.run(Lesson4Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Initialisation Completed");
        System.out.println(test);
        System.out.println(test1);
        System.out.println(test3);


        System.out.println(modelMapper);
    }
}
